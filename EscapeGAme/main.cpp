#include "EscapeGame.h"

int main(int argc, char* args[]) {
	EscapeGame game;

	game.Init();

	while (!game.Run()) {}

	game.ShutDown();
}