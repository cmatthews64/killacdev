#pragma once
#include "Game.h"

class EscapeGame : public Game
{
public:
	void OnInit();
	bool OnRun();
	void OnShutDown();

};