#pragma once
#include <SDL.h>
#include <stdio.h>


class Game {

public:
	bool Init();
	bool Run();
	void ShutDown();

	virtual void OnInit() = 0;
	virtual bool OnRun() = 0;
	virtual void OnShutDown() = 0;

private:
	SDL_Window* gWindow;

	//The surface contained by the window
	SDL_Surface* gScreenSurface;

	//The image we will load and show on the screen
	SDL_Surface* gHelloWorld;

	const int SCREEN_WIDTH ;
	const int SCREEN_HEIGHT;
};

